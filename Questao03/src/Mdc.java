public class Mdc {
    public static int calculaMDC(int num1, int num2) {
        if (num1 % num2 == 0) {
            return num2;
        }
        return calculaMDC(num2,(num1 % num2));
    }
}
