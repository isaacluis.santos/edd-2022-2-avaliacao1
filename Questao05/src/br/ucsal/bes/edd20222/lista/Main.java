package br.ucsal.bes.edd20222.lista;

import br.ucsal.bes.edd20222.lista.util.Lista;

import java.util.concurrent.ThreadLocalRandom;

public class Main {
    public static void main(String[] args) {
        Lista lista = new Lista();
        int min = 1;
        int max = 10;
        lista.inserir(ThreadLocalRandom.current().nextInt(min, max + 1));
        lista.inserir(ThreadLocalRandom.current().nextInt(min, max + 1));
        lista.inserir(ThreadLocalRandom.current().nextInt(min, max + 1));
        lista.inserir(ThreadLocalRandom.current().nextInt(min, max + 1));
        lista.inserir(ThreadLocalRandom.current().nextInt(min, max + 1));
        lista.inserir(ThreadLocalRandom.current().nextInt(min, max + 1));
        lista.inserir(ThreadLocalRandom.current().nextInt(min, max + 1));
        lista.inserir(ThreadLocalRandom.current().nextInt(min, max + 1));
        lista.inserir(ThreadLocalRandom.current().nextInt(min, max + 1));
        lista.inserir(ThreadLocalRandom.current().nextInt(min, max + 1));
        System.out.println(lista);
        Lista ordenada = lista.sort(lista);
        System.out.println(ordenada);
    }
}
