package br.ucsal.bes.edd20222.lista.util;

public class Lista {
   No inicio = null;
   No fim = null;
   int tamanho=0;
   public void inserir(int info){
       No no = new No();
       no.info = info;
       no.anterior = null;
       no.proximo = inicio;
       if (inicio!=null) {
           inicio.anterior = no;
       }
       inicio = no;
       if (tamanho==0) {
           fim=inicio;
       }
       tamanho++;
   }

   public Lista sort(Lista listaUnord) {
       Lista sorted = new Lista();
       No local = listaUnord.inicio;
       No aux = local;
       int auxInt = aux.info;
       int contadorDeLoopExterno=0, contadorDeLoopinterno=0;
       while (contadorDeLoopExterno!=listaUnord.tamanho) {
           while (local.proximo!=null) {
               if (local.proximo.info > auxInt) {
                   aux = local;
                   auxInt = aux.proximo.info;
                   contadorDeLoopinterno++;
               }
               local = local.proximo;
           }
           if (contadorDeLoopinterno==0) {
               sorted.inserir(aux.info);
               listaUnord.inicio = aux.proximo;
           } else {
               sorted.inserir(auxInt);
               aux.proximo = aux.proximo.proximo;
           }
           contadorDeLoopinterno=0;
           aux = local = listaUnord.inicio;
           if (aux!=null) {
               auxInt = aux.info;
           }
           contadorDeLoopExterno++;
       }
       return sorted;
   }

   @Override
   public String toString() {
       No local = inicio;
       String str ="";
       while (local!=null) {
           str += " " + local.info;
           local = local.proximo;
       }
       return str;
   }


}
