public class Somar {
    public static int somando(int limite) {
        if (limite == 0) {
            return 0;
        }
        return limite + somando(limite - 1);
    }
}
