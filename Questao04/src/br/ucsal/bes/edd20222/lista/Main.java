package br.ucsal.bes.edd20222.lista;

import br.ucsal.bes.edd20222.lista.util.Lista;

public class Main {
    public static void main(String[] args) {
        Lista l1 = new Lista();
        Lista l2 = new Lista();

        System.out.println(
                l1.inserir("A", 1)
                        + "\n" +
                l1.inserir("B", 1)
                        + "\n" +
                l1.inserir("C", 1)
                        + "\n" +
                l1.inserir("D", 1)
                        + "\n" +
                l1.inserir("E", 1)
                        + "\n" +
                l1.inserir("F", 1)
                        + "\n" +
                l1.inserir("G", 1)
                        + "\n" +
                l1.inserir("H", 1)
                        + "\n" +
                l1.inserir("P", 1)
                        + "\n" +
                l2.inserir("A", 2)
                        + "\n" +
                l2.inserir("X", 2)
                        + "\n" +
                l2.inserir("C", 2)
                        + "\n" +
                l2.inserir("D", 2)
                        + "\n" +
                l2.inserir("I", 2)
                        + "\n" +
                l2.inserir("F", 2)
                        + "\n" +
                l2.inserir("Z", 2)
        );

        Lista l3 = l1.comparar(l1, l2);
        System.out.println(l3);
    }
}
