package br.ucsal.bes.edd20222.lista.util;

public class Lista {
    No inicio = null;
    int tamanho=0;
    public String inserir(String info, int i){
        No no = new No();
        no.info = info;
        no.proximo = inicio;
        inicio = no;
        tamanho++;
        return info + " adicionado a lista "+i;
    }

    public Lista comparar(Lista lista1, Lista lista2) {
        No primeira = lista1.inicio;
        No segunda = lista2.inicio;
        Lista terceira = new Lista();
        while (primeira!=null) {
            while (segunda!=null) {
                if (primeira.info==segunda.info) {
                    terceira.inserir(primeira.info, 3);
                }
                segunda = segunda.proximo;
            }
            segunda = lista2.inicio;
            primeira = primeira.proximo;
        }
        return terceira;
    }

    @Override
    public String toString() {
        No local = inicio;
        String str ="";
        while (local!=null) {
            str += " " + local.info;
            local = local.proximo;
        }
        return str;
    }
}
