package interfaces;

public interface ContaBancariaInterface {
    double visualizarSaldo();
    double depositar(double montante);
    double sacar(double montante);
    double atualizar(double montante);
}
