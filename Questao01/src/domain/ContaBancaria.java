package domain;

import interfaces.ContaBancariaInterface;

public class ContaBancaria implements ContaBancariaInterface {

    double saldo;

    public ContaBancaria(double saldo) {
        this.saldo = saldo;
    }

    @Override
    public double visualizarSaldo() {
        return this.saldo;
    }

    @Override
    public double depositar(double montante) {
        return atualizar(montante);
    }

    @Override
    public double sacar(double montante) {
        return atualizar(-montante);
    }

    @Override
    public double atualizar(double montante) {
        return saldo += montante;
    }

    @Override
    public String toString() {
        return "ContaBancaria{" +
                "saldo=" + saldo +
                '}';
    }
}
