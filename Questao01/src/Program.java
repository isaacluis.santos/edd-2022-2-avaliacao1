import domain.ContaBancaria;

public class Program {
    public static void main(String[] args) {
        ContaBancaria cb = new ContaBancaria(100d);
        cb.sacar(100);
        cb.depositar(10);
        System.out.println(cb);
    }
}
